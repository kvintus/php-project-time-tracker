<?php include 'components/header.php'?>
<?php
    $date1 = new DateTime("2018-02-11 17:25:52");
    $date2 = new DateTime("2018-02-11 19:33:34");

    $interval = $date2->diff($date1);
    echo $interval->format("%h hours and %i minutes");
    echo "<br>";
    echo $date1->format("d.m.Y");
?>
<?php include 'components/footer.php'?>
