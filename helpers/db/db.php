<?php
    $connection = mysqli_connect('localhost', 'root', '', 'time-tracker-app');
    if (!$connection) {
        die('There\'s been a problem connecting to the Database');
    }
    /** Logs user in and hadles all the errors if any arise*/
    function logUserIn(string $email, string $password) {
        global $connection;
        $query = "SELECT * FROM developers WHERE email='$email'";
        $result = mysqli_query($connection, $query);

        if(!$result) {
            $_SESSION['login_error'] = (mysqli_error($connection));
            header('Location: login.php');
            die();
        }
        // There is no user with specified email
        if(mysqli_num_rows($result) === 0) {
            $_SESSION['login_error'] = 'There is no user with the email: '.$email;
            header('Location: login.php');
            die();
        }

        $dev = mysqli_fetch_assoc($result);
        if (hash('sha256', $password) === $dev['hash']){
            $_SESSION['user'] = array(
                'id' => $dev['id'],
                'email' => $dev['email']
            );
            unset($_SESSION['login_error']);
            header('Location: index.php');
            die();
        } else {
            $_SESSION['login_error'] = 'Incorect password!';
            header('Location: login.php');
            die();
        }
    }
    /** Gets all active projects of a logged-in user*/
    function getCurrentUsersActiveProjects(): array {
        global $connection;
        
        $dev_id = $_SESSION['user']['id'];
        $query = "SELECT name, id, image_url FROM projects WHERE lead_dev=". $dev_id ." AND finished_at IS NULL";
        $result = mysqli_query($connection, $query);

        if (!$result) {
            header("Location: error.php?message=" . mysqli_error($connection));
            die();
        }

        // If the user has no project yet JS on the track page will handle it...

        // Convert the mysqli array to normal array
        $resultArray = array();
        while ($row = mysqli_fetch_assoc($result)){
            array_push($resultArray, $row);
        }

        return $resultArray;
    }
    
    /** Start tracking time for current user and his specified project */
    function startTrackingTimeOnProject(int $projectID) {
        global $connection;
        $userID = $_SESSION['user']['id'];

        $query = "INSERT INTO work_logs (project_id, dev_id) VALUES ($projectID, $userID)";
        $result = mysqli_query($connection, $query);

        if (!$result) {
            header("Location: error.php?message=".mysqli_error($connection));
            die();
        }
    }
    /** Gets and returns the timer that hasn't been finished yet if there is no started timer it return null */
    function getStartedTimer() {
        global $connection;
        $dev_id = $_SESSION['user']['id'];
        $query = "SELECT * FROM work_logs WHERE dev_id=". $dev_id ." AND finished IS NULL";
        $result = mysqli_query($connection, $query);

        // If there's no pending timer return null
        if (mysqli_num_rows($result) === 0) {
            return NULL;
        }

        $result = mysqli_fetch_array($result);

        // Get the project
        $query = "SELECT id, name, image_url FROM projects WHERE id=".$result['project_id'];
        $project = mysqli_query($connection, $query);
        $project = mysqli_fetch_assoc($project);

        $result['project'] = $project;

        return $result; 
    }

    function stopTrackingOnCurrentTimer($summary) {
        global $connection;
        $query = "UPDATE work_logs SET finished=NOW(), summary='". $summary ."' WHERE id=". getStartedTimer()['id'];
        $result = mysqli_query($connection, $query);

        if(!isset($result)) {
            header("Location: error.php?message=".mysqli_error($connection));
            die();
        }
    }

    function getLastNWorkLogsOfCurrentUser($n = 10): array {
        global $connection;
        $query = "SELECT work_logs.start, work_logs.finished, work_logs.summary, work_logs.project_id, projects.name, projects.image_url";
        $query .= " FROM work_logs"; 
        $query .= " INNER JOIN projects ON work_logs.project_id=projects.id";
        $query .= " ORDER BY work_logs.id DESC LIMIT ".$n;
        $result = mysqli_query($connection, $query);

        if(!isset($result)) {
            header("Location: error.php?message=".mysqli_error($connection));
            die();
        }

        $final = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($final, $row);
        }

        return $final;
    }