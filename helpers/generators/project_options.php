<?php
    /** Generates a string of multiple HTML option tags */
    function generateCustomOptionsForProjects(array $projects): string {
        $result = '';

        foreach($projects as $project) {
            $result .= "<option class='circle' data-icon='". $project['image_url'] ."' value=". $project['id'] .">". $project['name'] ."</option>";
        }

        return $result;
    }