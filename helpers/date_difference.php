<?php
    function whenAndForHowLong(string $earlier,string $later):string {
        $date1 = new DateTime($earlier);
        $date2 = new DateTime($later);

        $interval = $date2->diff($date1);
        $intervalString = $interval->format("%h hours and %i minutes");
        $dateString = $date1->format("d.m.Y");
        
        return "At: ". $dateString ." for ".$intervalString;
    }