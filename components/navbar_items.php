<?php
    $navbar_items = array(
        'both' => array(),
        'logged_in' => array(
            'track' => array(
                'url' => 'tracking.php',
                'icon' => 'access_time',
                'text' => 'Track Work',
            ),
            'logout' => array(
                'url' => 'logout.php',
                'icon' => 'exit_to_app',
                'text' => 'Logout',
            ),
        ),
        'logged_out' => array(
            'login' => array(
                'url'=> 'login.php',
                'icon' => 'lock_open',
                'text' => 'Login',
            ),
        ),
    );

    function generateSingleNavbarListItemLi(array $item): string {
        return "<li><a href='" . $item['url'] . "'>" . $item['text'] . "<i class='material-icons'>" . $item['icon'] . "</i></a></li>";
    }

    /** Generates a string containg all HTML Li menu Items generated from passed-in array */
    function generateNavbarListItems(array $items): string {
        $tempWrapper = '';

        // Generating and assigning links that should display no matter what
        foreach ($items['both'] as $key => $item) {
            $tempWrapper .= generateSingleNavbarListItemLi($item);
        }

        // Generating and assigning links that should only when user is logged in
        if (isset($_SESSION['user'])) {
            foreach ($items['logged_in'] as $key => $item) {
                $tempWrapper .= generateSingleNavbarListItemLi($item);
            }
        }
        // Generating and assigning links that should only when user is logged out
        else {
            foreach ($items['logged_out'] as $key => $item) {
                $tempWrapper .= generateSingleNavbarListItemLi($item);
            }
        }

        return $tempWrapper;
    }
