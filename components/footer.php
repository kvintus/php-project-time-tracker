
</body>
</html>

<!-- Sets the title of the page -->
<script>
    let toAppend = '<?php isset($title) ? $title : ''?>';

    if (toAppend !== '') {
        document.title += ` | ${toAppend}`;
    }
</script>

<!-- Materialize and jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="static/js/functions.js"></script>

<script>
    $(".button-collapse").sideNav();
</script>