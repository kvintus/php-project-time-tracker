<?php 
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="static/images/favicon.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="static/css/style.css">
    <title>ProjectTracker</title>
</head>
<body>

  <nav class="teal accent-4">
    <!-- Hamburger menu icon -->
    <a href="#" data-activates="slide-out" class="button-collapse left"><i class="material-icons">menu</i></a>

    
    <div class="nav-wrapper">
        <!-- Logo -->
        <a href="index.php" class="brand-logo">TimeTracker <img src="static/images/favicon.png" alt="Logo"></a>

        <!-- Main Navigation -->
        <ul id="nav-mobile" class="right hide-on-med-and-down">           
            <?php include 'components/navbar_items.php' ?>
            <?php echo generateNavbarListItems($navbar_items) ?>
        </ul>

        <!-- Mobile Navigation -->
        <ul id="slide-out" class="side-nav">
            <div style="margin-top: 1rem;"></div>  <!-- Spacing from the Top -->
            <?php echo generateNavbarListItems($navbar_items) ?>
        </ul>
    </div>
</nav>

