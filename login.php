<?php include 'components/header.php';?>
<?php include 'helpers/db/db.php'?>

<?php
    if (isset($_POST['login']) && isset($_POST['email']) && isset($_POST['password'])) {
        $email = mysqli_real_escape_string($connection, $_POST['email']);
        $password = mysqli_real_escape_string($connection, $_POST['password']);
        logUserIn($email, $password);
    }
?>



    <div style="display: flex; justify-content: center;" class="container">
        <div class="card-wrapper">
            <?php if (isset($_SESSION['login_error'])) { ?>
            <div id="card-alert" class="card red">
                <div class="card-content white-text">
                    <p style="display: inline-block"><?php echo $_SESSION['login_error']?></p>
                    <i style="float: right; cursor: pointer;" class="material-icons">clear</i>
                </div>
            </div>
            <?php } ?>
            <div id="login-card" class="col card z-depth-4">
                <form action="login.php" method="post" id="login-form">
                    <div class="card-content">
                        <span class="card-title">Login</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="email" class="validate" name="email" id="email" required/>
                                <label for="email" data-error="Desired format: example@example.com">Email address</label>
                            </div>
                            <div class="input-field col s12">
                                <input type="password" class="validate" name="password" id="password" required/>
                                <label for="password">Password </label>
                            </div>
                        </div>
                    </div>
                    <div class="card-action right-align">
                        <button type="reset" id="reset" class="btn-flat grey-text waves-effect">Reset</button>
                        <button type="submit" class="btn teal accent-4 waves-effect waves-light" name="login">Login</button>
                    </div>
                </form>
            </div>
        </div>


    </div>

    <?php include 'components/footer.php' ?>