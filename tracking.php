<?php include 'components/header.php'?>
<?php include 'helpers/db/db.php'?>
<?php include 'helpers/generators/project_options.php'?>
<?php include 'helpers/date_difference.php'?>

<?php
    // Check if the user has a started timer
    $timer = getStartedTimer();
    if (!is_null($timer)) {
        echo "<script type='text/javascript'>const timer_start = '". $timer['start'] ."'</script>"; // Set the timer start for JS
    }
    
    if(isset($_POST['start_tracking'])) {
        // Just to be sure
        if (!isset($_POST['project'])){
            header("Location: error.php?message=You cannot start timer without setting project on it");
        }
        // Start the timer
        startTrackingTimeOnProject($_POST['project']);
        header("Location: tracking.php");
    }

    if(isset($_POST['stop_tracking'])) {
        if (!isset($_POST['summary'])){
            header("Location: error.php?message=You cannot start timer without setting project on it");
        }

        stopTrackingOnCurrentTimer($_POST['summary']);
        header("Location: tracking.php");
    }
    
?>

    <div class="container">
        <div class="card">
            <div class="card-content">
                <div class="heading-wrapper">
                    <h1 id="time-heading" class="card-title">Time
                        <?php echo (!is_null($timer)) ? "spent working on: " : ''; ?>
                    </h1>
                    <?php if (!is_null($timer)) { ?>
                        <a class="project-link" href="project.php?project=<?php echo $timer['project']['id'] ?>">
                            <div class="project-group z-depth-2">
                                <h4><?php echo $timer['project']['name'] ?></h4>
                                <div style="max-width: 50px;" class="img-log-wrapper">
                                    <img style="margin-left: 15px;" src="<?php echo $timer['project']['image_url'] ?>" alt="">
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                </div>
                <div id="time-wrapper" class="row">
                    <h1 id="time-display">00:00:00</h1>
                </div>
                <div class="row">
                    
                    <!-- Start Timer Form -->
                    <?php if (is_null($timer)) { ?>
                    <form action="tracking.php" method="post">
                        <div class="input-field">
                            <select name="project" required>
                                <option value="" disabled selected>Choose your project</option>
                                <?php echo generateCustomOptionsForProjects(getCurrentUsersActiveProjects()) ?>
                            </select>
                        </div>
                        <div class="btn-wrapper">
                            <button type="submit" name="start_tracking" class="add-btn btn blue darken-3">
                                <i class="material-icons left-5">alarm_add</i>Start Working</button>
                        </div>
                    </form>
                    <?php } else { ?>

                    <!-- Stop work Form -->
                    <form id="stop-form" class="row" action="tracking.php" method="post">
                        <div class="input-field">
                            <textarea id="summary" class="materialize-textarea" name="summary" required></textarea>
                            <label for="summary">Summary</label>
                        </div>
                        <div class="stop-btn btn-wrapper">
                            <button type="submit" name="stop_tracking" class="btn red darken-3">
                                <i class="material-icons left-5">weekend</i>
                                Stop Working
                            </button>
                        </div>
                    </form>
                    <?php } ?>


                </div>
            </div>
        </div>

        <!-- Logs -->
        <h1 id="logs-heading" class="center">Logs
            <i class="material-icons">history</i>
        </h1>
        <ul class="collapsible" data-collapsible="accordion">
            <!-- Generating Log Items-->
            <?php $logs = getLastNWorkLogsOfCurrentUser(); ?>
            <?php foreach($logs as $log) { ?>
                <li>
                    <div class="collapsible-header">
                        <div class="img-log-wrapper">
                            <img src="<?php echo $log['image_url'] ?>" alt="project image">
                        </div>
                        <span><?php echo $log['name'] ?></span>
                        <span class="time-info"><?php echo whenAndForHowLong($log['start'], $log['finished']) ?></span>
                    </div>
                    <div class="collapsible-body">
                        <span><?php echo $log['summary'] ?></span>
                    </div>
                </li>
            <?php } ?>
        </ul>

    </div>

    <?php include 'components/footer.php'?>
    <script src="http://momentjs.com/downloads/moment.min.js"></script>
    <script>
        $(document).ready(function () {
            // Selection
            $('select').material_select();
            // Start the timer
            if (!(typeof timer_start === "undefined")) {
                let timerElement = document.getElementById('time-display');
                setInterval(function () {
                    let now = new Date();
                    timerElement.textContent = moment(moment.utc() - moment.utc(timer_start)).format("HH:mm:ss");
                }, 500);
            }
        });
    </script>